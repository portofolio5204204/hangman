const wordEl = document.getElementById("word");
const wrongLettersEl = document.getElementById("wrong-letters");
const playAgainBtn = document.getElementById("play-button");
const popup = document.getElementById("popup-container");
const notification = document.getElementById("notification-container");
const finalMessage = document.getElementById("final-message");

const figureParts = document.querySelectorAll(".figure-part");

const apiURL = "https://random-word-api.herokuapp.com/all";

let words = [];
let selectedWord = "";

//const words = ['application', 'programming', 'wizard', 'interface'];

//let selectedWord = words[Math.floor(Math.random() * words.length)];

const correctLetters = [];
const wrongLetters = [];

// Fetch word from API
async function searchWords() {
  const res = await fetch(`${apiURL}`);
  words = await res.json();
  selectedWord = words[Math.floor(Math.random() * words.length)];

  displayWord();
} // same function but with async await

// Show hidden word
function displayWord() {
  wordEl.innerHTML = `${selectedWord
    .split("")
    .map(
      (letter) => `
            <span class="letter">
                ${correctLetters.includes(letter) ? letter : ""}
            </span>`
    )
    .join("")}`;

  const innerWord = wordEl.innerText.replace(/\n/g, ""); // replaces the new line character with an empty string globally, wherever it is found

  if (innerWord === selectedWord) {
    // conditional for the typed word to be equal to the game word
    finalMessage.innerText = "Congratulations! You won!"; // Display the text
    popup.style.display = "flex"; // set the display style which makes the popup visible
  }
}

// Update the wrong letters html
function updateWrongLettersEl() {
  wrongLettersEl.innerHTML = `${wrongLetters.length > 0 ? "<p>Wrong:</p>" : ""} 
    ${wrongLetters.map((letter) => `<span>${letter}</span>`)}`; // Displays the wrong letters

  figureParts.forEach((part, index) => {
    // loop through the hangman array of elements
    const errors = wrongLetters.length;

    if (index < errors) {
      // display a hangman part for each wrong letter
      part.style.display = "block";
    } else {
      part.style.display = "none";
    }
  });

  // Check if lost
  if (wrongLetters.length === figureParts.length) {
    finalMessage.innerText = "Unfortunately you lost. :(";
    popup.style.display = "flex";
  }
}

// SHow notification
function showNotification() {
  notification.classList.add("show"); // adds the show class to the element

  setTimeout(() => {
    notification.classList.remove("show"); // removes the show class after 2s
  }, 2000);
}

// Event listener for keyboard strokes
window.addEventListener("keydown", (e) => {
  // listen for keyboard
  if (e.keyCode >= 65 && e.keyCode <= 90) {
    // check that only letters on the keyboard matter and no other random characters
    const letter = e.key;

    if (selectedWord.includes(letter)) {
      if (!correctLetters.includes(letter)) {
        correctLetters.push(letter);

        displayWord();
      } else {
        showNotification();
      }
    } else {
      if (!wrongLetters.includes(letter)) {
        wrongLetters.push(letter);

        updateWrongLettersEl();
      } else {
        showNotification();
      }
    }
  }
});

// Restart game functionality
playAgainBtn.addEventListener("click", () => {
  // empty arrays
  correctLetters.splice(0);
  wrongLetters.splice(0);

  selectedWord = words[Math.floor(Math.random() * words.length)];

  displayWord();

  updateWrongLettersEl();

  popup.style.display = "none";
});

//displayWord();
searchWords();
